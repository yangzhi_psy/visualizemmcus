#library(shiny)
shinyUI(fluidPage(
  titlePanel("Interactive Explorer for Structural-Functional Covariance", windowTitle="Structural-Functional Covariance"),
  sidebarLayout(
    sidebarPanel(

      textOutput("clicktext"),
      textOutput("text") ,
      br(),
      br(),

      strong("Reference:"),
      p("Yang Z*, Qiu J, Wang P, Liu R, Zuo X-N*, Learning brain structure-function associations from large-scale neuroimaging data (under review)")
    ),
    mainPanel(
      imageOutput("img", width="750px", height="800px", 
                 click=hoverOpts(id="click", clip=TRUE, nullOutside=TRUE),
                 hover=hoverOpts(id="plot_hover", delay=5, 
                                 delayType="debounce", clip=TRUE, nullOutside=TRUE))
      ),
    position = "right",
    fluid = TRUE)
))
