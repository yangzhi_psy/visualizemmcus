function varargout = testGUI(varargin)
% TESTGUI MATLAB code for testGUI.fig
%      TESTGUI, by itself, creates a new TESTGUI or raises the existing
%      singleton*.
%
%      H = TESTGUI returns the handle to a new TESTGUI or the handle to
%      the existing singleton*.
%
%      TESTGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in TESTGUI.M with the given input arguments.
%
%      TESTGUI('Property','Value',...) creates a new TESTGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before testGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to testGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help testGUI

% Last Modified by GUIDE v2.5 27-Oct-2014 23:22:29

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @testGUI_OpeningFcn, ...
                   'gui_OutputFcn',  @testGUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before testGUI is made visible.
function testGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to testGUI (see VARARGIN)

% Choose default command line output for testGUI
handles.output = hObject;

% load pictures
load full_figs/scaleFullind.mat;
load cmap_purple_lightblue_red.mat;
nCp = 39;
handles.pics =uint8(zeros(750, 800,3, nCp));
handles.cmap_purple_lightblue_red = cmap_purple_lightblue_red;
handles.scaleFullind = scaleFullind;
for cp = 1:nCp+1
    handles.pics(:,:,:,cp) = imread (sprintf ('full_figs/scale.full.covGraph%d.jpg', cp-1));
end

% show first component as initialization
axes(handles.axes5);
haxes5 = imagesc(handles.pics(:,:,:,2));
colormap(handles.axes5, handles.cmap_purple_lightblue_red);
set(handles.axes5,'YTick',[]);
set(handles.axes5,'XTick',[]);
box off

% change the pointer into crosshair
set (hObject, 'pointer', 'crosshair');
set(handles.text1, 'string', sprintf('The current location\n does not contribute to any\n Covariance Graph'));
set(handles.text1, 'foregroundcolor', [1 0 0]);

% Update handles structure
guidata(hObject, handles);
% reset the callback function of axes5
set(haxes5,'ButtonDownFcn',@axes5_ButtonDownFcn)


% UIWAIT makes testGUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = testGUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes during object creation, after setting all properties.
function axes5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axes5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate axes5
box off;

% --- Executes on mouse press over axes background.
function axes5_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to axes5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



% --- Executes on mouse press over figure background, over a disabled or
% --- inactive control, or over an axes background.
function figure1_WindowButtonUpFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles = guidata(gcf);
point = get(handles.axes5,'CurrentPoint'); % get click position from the axes object
coord = round ([point(1,2), point(1,1)]);
if (coord(1)>0) && (coord(1)<751) && (coord(2)>0) && (coord(2)<801)
    cpIdx = handles.scaleFullind(coord(1), coord(2));
    if cpIdx ~= 0
        % show covgraph for the cp
        axes(handles.axes5);
        haxes5 = imagesc(handles.pics(:,:,:,cpIdx+1));
        colormap(handles.axes5, handles.cmap_purple_lightblue_red);
        set(handles.text1, 'string', sprintf('The current location\n maximally contribute to\n Covariance Graph %d', cpIdx));
        set(handles.text1, 'foregroundcolor', [0 0.7 0]);
        % reset the callback function of axes5
        set(haxes5,'ButtonDownFcn',@axes5_ButtonDownFcn);
    else
        axes(handles.axes5);
        haxes5 = imagesc(handles.pics(:,:,:,1));
        colormap(handles.axes5, handles.cmap_purple_lightblue_red);
        
        set(handles.text1, 'string', sprintf('The current location\n does not contribute to any\n Covariance Graph'));
        set(handles.text1, 'foregroundcolor', [1 0 0]);
        % reset the callback function of axes5
        set(haxes5,'ButtonDownFcn',@axes5_ButtonDownFcn);
    end
    set(handles.axes5,'YTick',[]);
    set(handles.axes5,'XTick',[]);
    box off
end


% --- Executes on mouse motion over figure - except title and menu.
function figure1_WindowButtonMotionFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles = guidata(gcf);
point = get(handles.axes5,'CurrentPoint'); % get click position from the axes object
coord = round ([point(1,2), point(1,1)]);
if (coord(1)>0) && (coord(1)<751) && (coord(2)>0) && (coord(2)<801)
    cpIdx = handles.scaleFullind(coord(1), coord(2));
    if cpIdx ~= 0
        set(handles.text1, 'string', sprintf('The current location\n maximally contribute to\n Covariance Graph %d', cpIdx));
        set(handles.text1, 'foregroundcolor', [0 0.7 0]);
    else
        set(handles.text1, 'string', sprintf('The current location\n does not contribute to any\n Covariance Graph'));
        set(handles.text1, 'foregroundcolor', [1 0 0]);
    end
end
